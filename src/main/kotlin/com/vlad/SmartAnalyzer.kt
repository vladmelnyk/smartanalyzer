package com.vlad

import javafx.util.Pair
import org.jsoup.Jsoup
import org.jsoup.nodes.Attribute
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import java.io.File
import java.io.IOException
import java.util.*

private const val CHARSET_NAME = "utf8"
private const val PATH_SEPARATOR = " > "

object SmartAnalyzer {

    fun run(originFile: File, sampleFile: File, attrKey: String, targetElementId: String) {
        val originDocument = getDocument(originFile)
        val sampleDocument = getDocument(sampleFile)
        val elementToFind = findElementByAttributeValue(originDocument, Attribute(attrKey, targetElementId))
        val similarElement = findSimilarElement(sampleDocument, elementToFind)
        val path = getPath(similarElement.key, StringBuilder())
        LOG.info("Path to element is: $path")
    }

    private fun getDocument(htmlFile: File): Document {
        return try {
            Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.absolutePath)
        } catch (e: IOException) {
            LOG.severe("Error reading [$htmlFile.absolutePath] file \n $e")
            throw RuntimeException("Cannot parse file $htmlFile")
        }
    }

    private fun findElementByAttributeValue(root: Document, attribute: Attribute): Element {
        return root.getElementsByAttributeValue(attribute.key, attribute.value)?.first()
                ?: throw RuntimeException("Cannot find element by attribute")
    }

    private fun findSimilarElement(document: Document, elementToFind: Element): Pair<Element, List<Attribute>> {
        return document.allElements
                .filter { it.tagName().equals(elementToFind.tagName(), ignoreCase = true) }
                .map { Pair(it, findCommonAttrs(it, elementToFind)) }
                .maxWith(Comparator.comparing<Pair<Element, List<Attribute>>, Int> { it.value.size })
                ?: throw RuntimeException("Cannot find similar element for document $document and elementToFind $elementToFind")
    }

    private fun getPath(element: Element, path: StringBuilder): StringBuilder {
        var currentTag = element.tagName()
        val parent = element.parent()
        if (parent != null) {
            currentTag = PATH_SEPARATOR.plus(currentTag)
            val children = parent.children().filter { it.tagName() == element.tagName() }
            if (children.size > 1) {
                currentTag += "[${(0..children.size).first { children[it] == element } + 1}]"
            }
            getPath(parent, path)
        }
        path.append(currentTag)
        return path
    }

    private fun findCommonAttrs(first: Element, second: Element): List<Attribute> {
        return first.attributes().filter { second.attributes().toSet().contains(it) }
    }

}
