package com.vlad

import java.io.File
import java.util.logging.Logger

val LOG = Logger.getAnonymousLogger()

fun main(args: Array<String>) {
    var targetElementId = "make-everything-ok-button"

    when (args.size) {
        3 -> {
            targetElementId = args[2]
        }
        0, 1 -> {
            LOG.severe("some filename args are not specified")
            return
        }
        2 -> {
        }
        else -> {
            LOG.severe("too many args")
            return
        }
    }
    LOG.info("Input origin file path ${args[0]}, target file path ${args[1]}, target element id: $targetElementId")

    SmartAnalyzer.run(File(args[0]), File(args[1]), "id", targetElementId)


}