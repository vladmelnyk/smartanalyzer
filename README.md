This is Smart Analyzer - a simple web crawler that locates a user-selected element on a web site.

To simply run from packaged binary, run 

```java -jar SmartAnalyzer.jar  ./samples/sample-0-origin.html ./samples/sample-1-evil-gemini.html wrapper```

Expect output like: 

``` 
   июн 07, 2018 3:50:58 AM com.vlad.MainKt main
   INFO: Input origin file path ./samples/sample-0-origin.html, target file path ./samples/sample-1-evil-gemini.html, target element id: wrapper
   июн 07, 2018 3:50:58 AM com.vlad.SmartAnalyzer run
   INFO: Path to element is: #root > html > body > div
```

Where arguments are as follows:
 * original document
 * target document
 * (Optional) targetElementId
 
